import {gql} from "@apollo/client";

export const JournaalpostFragment = gql`
    fragment Journaalpost on Journaalpost {
        id
    }
`;