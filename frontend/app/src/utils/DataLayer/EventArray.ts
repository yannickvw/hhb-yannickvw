export type DataLayerEvent = {
	event: string,
	payload?: any
};
export type EventArray = DataLayerEvent[];