import React from "react";
import MenuIcon from "./MenuIcon";

export default {
	title: "Huishoudboekje/MenuIcon",
	component: MenuIcon,
	argTypes: {},
};

export const Default = () => {
	return (
		<MenuIcon />
	);
};
Default.args = {};